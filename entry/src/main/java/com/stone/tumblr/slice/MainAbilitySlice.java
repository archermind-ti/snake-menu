package com.stone.tumblr.slice;

import com.stone.tumblr.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;

public class MainAbilitySlice extends AbilitySlice {

    private TumblrLayout flingContainer;

    private BaseAdapter mProvider;

    private ArrayList<Integer> al;
    private ArrayList<Integer> albg;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        flingContainer = (TumblrLayout) findComponentById(ResourceTable.Id_frame);
        al = new ArrayList<>();
        al.add(ResourceTable.Media_image1);
        al.add(ResourceTable.Media_image2);
        al.add(ResourceTable.Media_image3);
        al.add(ResourceTable.Media_image4);
        al.add(ResourceTable.Media_image5);
        al.add(ResourceTable.Media_image6);

        albg = new ArrayList<>();
        albg.add(ResourceTable.Graphic_bg_image);
        albg.add(ResourceTable.Graphic_bg_image1);
        albg.add(ResourceTable.Graphic_bg_image2);
        albg.add(ResourceTable.Graphic_bg_image3);
        albg.add(ResourceTable.Graphic_bg_image4);
        albg.add(ResourceTable.Graphic_bg_image5);

        mProvider = new BaseAdapter(this);
        mProvider.setData(al,albg);
        //其他所需要的的布局文件在此布局中编写 父布局随意写什么类型都可以
        DirectionalLayout directionalLayout=new DirectionalLayout(this);
        ComponentContainer.LayoutConfig layoutConfig=new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,ComponentContainer.LayoutConfig.MATCH_PARENT);
        directionalLayout.setLayoutConfig(layoutConfig);
        ShapeElement shapeElement=new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(225,125,110));
        directionalLayout.setBackground(shapeElement);
        flingContainer.setMaxVisible(al.size());
        flingContainer.setMinStackInAdapter(al.size());
        flingContainer.setItemProvider(mProvider);
        flingContainer.addComponent(directionalLayout,0);
        flingContainer.setmFrameFulei(directionalLayout);
        flingContainer.setOnItemClickListener(new TumblrLayout.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                LogUtil.debug("LIST", "onItemClicked object!");
            }
        });
        ListContainer mListConTainer=new ListContainer(this);
        mListConTainer.setLayoutConfig(new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,DirectionalLayout.LayoutConfig.MATCH_PARENT));
        directionalLayout.addComponent(mListConTainer);
        ArrayList<Integer> ll=new ArrayList<>();
        for(int i=0;i<20;i++){
            ll.add(i);
        }
        ShapeElement shapeElement1=new ShapeElement();
        shapeElement1.setRgbColor(new RgbColor(255,255,255));
        mListConTainer.setBackground(shapeElement1);
        ListAdapter listAdapter=new ListAdapter(this);
        listAdapter.setData(ll);
        mListConTainer.setItemProvider(listAdapter);
        mListConTainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                new ToastDialog(MainAbilitySlice.this).setContentText("list click " +i).show();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
