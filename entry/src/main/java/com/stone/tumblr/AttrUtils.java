/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stone.tumblr;

import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * Get attribute value from resource file
 */
public class AttrUtils {
    /**
     * Get an int value from resource file
     *
     * @param attrs Attribute Set
     * @param name Attribute name
     * @param defaultValue Default value
     * @return an int value
     */
    public static int getIntFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name) != null && attrs.getAttr(name).isPresent()) {
                value = attrs.getAttr(name).get().getIntegerValue();
            }
        } catch (NoSuchElementException e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get a float value from resource file
     *
     * @param attrs Attribute Set
     * @param name Attribute name
     * @param defaultValue Default value
     * @return a float value
     */
    public static float getFloatFromAttr(AttrSet attrs, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrs.getAttr(name) != null && attrs.getAttr(name).isPresent()) {
                value = attrs.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get a boolean value from resource file
     *
     * @param attrs Attribute Set
     * @param name Attribute name
     * @param defaultValue Default value
     * @return a boolean value
     */
    public static boolean getBooleanFromAttr(AttrSet attrs, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrs.getAttr(name) != null && attrs.getAttr(name).isPresent()) {
                value = attrs.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get a long value from resource file
     *
     * @param attrs Attribute Set
     * @param name Attribute name
     * @param defaultValue Default value
     * @return a long value
     */
    public static long getLongFromAttr(AttrSet attrs, String name, long defaultValue) {
        long value = defaultValue;
        try {
            if (attrs.getAttr(name) != null && attrs.getAttr(name).isPresent()) {
                value = attrs.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get color value from resource file
     *
     * @param attrs Attribute Set
     * @param name Attribute name
     * @param defaultValue Default value
     * @return color value
     */
    public static int getColorFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name) != null && attrs.getAttr(name).isPresent()) {
                value = attrs.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get dimension from resource file
     *
     * @param attrs Attribute Set
     * @param name Attribute name
     * @param defaultValue Default value
     * @return Dimension
     */
    public static int getDimensionFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name) != null && attrs.getAttr(name).isPresent()) {
                value = attrs.getAttr(name).get().getDimensionValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * To Load string from the resource file
     *
     * @param attrs Attribute Set
     * @param name Attribute name
     * @param defaultValue Default value
     * @return String
     */
    public static String getStringFromAttr(AttrSet attrs, String name, String defaultValue) {
        String value = defaultValue;
        try {
            if (attrs.getAttr(name) != null && attrs.getAttr(name).isPresent()) {
                value = attrs.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * To load PixelMap from resource file
     *
     * @param context Context
     * @param id Resource ID
     * @return PixelMap
     */
    public static PixelMap getPixelMap(Context context, int id) {
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/png";
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        try {
            Resource asset = context.getResourceManager().getResource(id);
            ImageSource source = ImageSource.create(asset, options);
            return source.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return null;
    }
}
