# Snake_menu

#### 简介
模仿Tumblr的菜单，拖动动画就像一条蛇

#### 功能
跟随手势拖动的“贪吃蛇”动画控件

#### 演示
![demo预览](https://gitee.com/archermind-ti/snake-menu/raw/master/capture1.gif "在这里输入图片标题")

#### 集成

1.  下载源码工程使用
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    testImplementation 'junit:junit:4.13'
}
```
#### 使用说明

xml文件配置
```
<com.stone.tumblr.TumblrLayout
        ohos:id="$+id:frame"
        ohos:width="match_parent"
        ohos:height="match_parent"
        ohos:align_parent_bottom="true"
        ohos:align_parent_right="true"
        ohos:background_element="#00000000"/>
```
定义你需要的布局文件Add,也可引入xml布局文件加入
```
 DirectionalLayout directionalLayout=new DirectionalLayout(this);
        ComponentContainer.LayoutConfig layoutConfig=new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,ComponentContainer.LayoutConfig.MATCH_PARENT);
        directionalLayout.setLayoutConfig(layoutConfig);
        ShapeElement shapeElement=new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(225,125,110));
        directionalLayout.setBackground(shapeElement);
  tumblrLayout.addComponent(directionalLayout,0);
```
在directionalLayout自由添加你所需要的样式控件
```
ListContainer mListConTainer=new ListContainer(this);
        mListConTainer.setLayoutConfig(new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,DirectionalLayout.LayoutConfig.MATCH_PARENT));
        directionalLayout.addComponent(mListConTainer);
```
#### 编译说明
1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击Run运行即可（真机运行可能需要配置签名）


#### 版本迭代
- v1.0   初始版本
[changelog](https://gitee.com/archermind-ti/snake-menu/blob/master/changelog.md)

#### License

```
Copyright 2016, xmuSistone

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
